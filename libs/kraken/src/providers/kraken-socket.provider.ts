import { Provider } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import * as WebSocket from 'ws';

import { kraken } from '../config';
import { KRAKEN_SOCKET } from '../tokens';

export const KrakenSocketProvider: Provider = {
  provide: KRAKEN_SOCKET,
  useFactory: (config: ConfigType<typeof kraken>) => {
    return new WebSocket(config.webSocketConnectionUrl);
  },
  inject: [kraken.KEY],
};
