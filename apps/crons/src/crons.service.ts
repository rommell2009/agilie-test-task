import { Inject, Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';

import { PRISMA, PrismaService } from '@app/db';
import { IKrakenService, KRAKEN_SERVICE } from '@app/kraken';

@Injectable()
export class CronsService {
  private readonly logger = new Logger(CronsService.name);

  constructor(
    @Inject(PRISMA)
    private readonly prismaService: PrismaService,

    @Inject(KRAKEN_SERVICE)
    private readonly krakenService: IKrakenService,
  ) {}

  @Cron('0 0 * * *', { name: 'check-balances' })
  async checkBalances() {
    this.logger.verbose(`Cron check-balances started`);

    const accounts = await this.prismaService.account.findMany();

    for (const account of accounts) {
      const rate = await this.krakenService.getRate(
        `${account.cryptoCurrency}/${account.fiatCurrency}`,
      );

      if (!rate) {
        this.logger.warn(
          `There is no rate for pair ${account.cryptoCurrency}/${account.fiatCurrency}`,
        );
      }

      await this.prismaService.dailyBalance.create({
        data: {
          accountId: account.id,
          cryptoCurrency: account.cryptoCurrency,
          fiatCurrency: account.fiatCurrency,
          rate,
          balanceInCrypto: account.cryptoBalance,
          balanceInFiat: account.fiatBalance,
          totalBalanceInCrypto:
            account.cryptoBalance + account.fiatBalance / rate,
        },
      });
    }
  }
}
