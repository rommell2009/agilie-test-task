export interface IKrakenService {
  getRate(pair: string): Promise<number>;
}
