import { registerAs } from '@nestjs/config';

export const dbConfig = registerAs('db', () => ({
  redis: {
    connectionString: process.env.REDIS_CONNECTION_STRING,
  },
  prisma: {
    connectionString: process.env.POSTGRE_CONNECTION_STRING,
  },
}));
