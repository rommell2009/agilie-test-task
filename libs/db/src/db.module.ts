import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

import { dbConfig } from './config';
import { DB_PROVIDERS } from './providers';

@Global()
@Module({
  imports: [ConfigModule.forFeature(dbConfig)],
  providers: [...DB_PROVIDERS],
  exports: [...DB_PROVIDERS],
})
export class DBModule {}
