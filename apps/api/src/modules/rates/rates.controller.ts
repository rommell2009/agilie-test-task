import { Controller, Get, Inject, Query } from '@nestjs/common';

import { GetRateDto } from './dto';
import { IRatesService } from './interfaces';
import { RATES_SERVICE } from './tokens';

@Controller('rates')
export class RatesController {
  constructor(
    @Inject(RATES_SERVICE)
    private readonly service: IRatesService,
  ) {}

  @Get()
  async getRate(@Query() dto: GetRateDto) {
    return this.service.getRate(dto.cryptoCurrency, dto.fiatCurrency);
  }
}
