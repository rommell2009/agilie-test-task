import { Module } from '@nestjs/common';

import { KrakenModule } from '@app/kraken';

import { RatesController } from './rates.controller';
import { RatesService } from './rates.service';
import { RATES_SERVICE } from './tokens';

@Module({
  imports: [KrakenModule],
  controllers: [RatesController],
  providers: [{ provide: RATES_SERVICE, useClass: RatesService }],
})
export class RatesModule {}
