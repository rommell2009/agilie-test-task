import { config } from 'dotenv';

config();

class Environment {
  get port(): number {
    return parseInt(process.env.PORT || '80');
  }

  get isProduction(): boolean {
    return process.env.NODE_ENV === 'production';
  }
}

export const environment = new Environment();
