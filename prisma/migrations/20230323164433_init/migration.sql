-- CreateEnum
CREATE TYPE "currency_types" AS ENUM ('CRYPTO', 'FIAT');

-- CreateTable
CREATE TABLE "currency" (
    "code" TEXT NOT NULL,
    "name" TEXT NOT NULL,
    "type" "currency_types" NOT NULL DEFAULT 'CRYPTO',
    "active" BOOLEAN NOT NULL DEFAULT true,
    "precision" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "currency_pkey" PRIMARY KEY ("code")
);
