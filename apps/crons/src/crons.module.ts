import { Module } from '@nestjs/common';
import { ScheduleModule } from '@nestjs/schedule';

import { CoreModule } from '@app/core';
import { KrakenModule } from '@app/kraken';
import { DBModule } from '@app/db';

import { CronsService } from './crons.service';

@Module({
  imports: [
    CoreModule.forRoot(),
    DBModule,
    KrakenModule,
    ScheduleModule.forRoot(),
  ],
  providers: [CronsService],
})
export class CronsModule {}
