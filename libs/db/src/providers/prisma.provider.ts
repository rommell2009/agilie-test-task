import { Provider } from '@nestjs/common';
import { PrismaService } from '../services';

import { PRISMA } from '../tokens';

export const PrismaProvider: Provider = {
  provide: PRISMA,
  useClass: PrismaService,
};
