import { Currency, CurrencyTypes } from '@prisma/client';

export interface ICurrenciesService {
  getCurrencies(type: CurrencyTypes): Promise<Currency[]>;
}
