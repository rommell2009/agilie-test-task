import { Module } from '@nestjs/common';

import { CurrenciesController } from './currencies.controller';
import { CurrenciesService } from './currencies.service';
import { CURRENCIES_SERVICE } from './tokens';

@Module({
  controllers: [CurrenciesController],
  providers: [{ provide: CURRENCIES_SERVICE, useClass: CurrenciesService }],
})
export class CurrenciesModule {}
