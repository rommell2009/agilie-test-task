import { PrismaProvider } from './prisma.provider';
import { RedisProvider } from './redis.provider';

export const DB_PROVIDERS = [RedisProvider, PrismaProvider];
