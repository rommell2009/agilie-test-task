import { KrakenServiceProvider } from './kraken-service.provider';
import { KrakenSocketProvider } from './kraken-socket.provider';

export const KRAKEN_PROVIDERS = [KrakenSocketProvider, KrakenServiceProvider];
