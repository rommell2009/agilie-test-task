import { IsPositive } from 'class-validator';

export class BaseListDto {
  @IsPositive()
  limit: number;

  @IsPositive()
  skip: number;
}
