import { Module } from '@nestjs/common';

import { CoreModule } from '@app/core';
import { DBModule } from '@app/db';

import { CurrenciesModule, RatesModule } from './modules';

@Module({
  imports: [CoreModule.forRoot(), DBModule, CurrenciesModule, RatesModule],
})
export class ApiModule {}
