FROM node:16-alpine

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm ci 

COPY . .

ARG APP=api

RUN npm run build ${APP}

RUN rm -rf apps && rm -rf libs

ENV APP ${APP}

CMD node dist/apps/${APP}/main
