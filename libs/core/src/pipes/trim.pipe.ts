import { PipeTransform, ArgumentMetadata } from '@nestjs/common';

export class TrimPipe implements PipeTransform<any> {
  transform(value: any, { type }: ArgumentMetadata): any {
    if (type !== 'body') {
      return value;
    }

    return this.trim(value);
  }

  private trim(data: any) {
    switch (typeof data) {
      case 'object':
        if (!data) return data;

        for (const [k, v] of Object.entries(data)) {
          data[k] = this.trim(v);
        }
        return data;
      case 'string':
        return data.trim();
      default:
        return data;
    }
  }
}
