import { BadRequestException, Inject, Injectable } from '@nestjs/common';

import { IKrakenService, KRAKEN_SERVICE } from '@app/kraken';
import { PRISMA, PrismaService } from '@app/db';

import { IRate, IRatesService } from './interfaces';

@Injectable()
export class RatesService implements IRatesService {
  constructor(
    @Inject(KRAKEN_SERVICE)
    private readonly krakenService: IKrakenService,

    @Inject(PRISMA)
    private readonly prismaService: PrismaService,
  ) {}

  async getRate(cryptoCurrency: string, fiatCurrency: string): Promise<IRate> {
    const currencies = await this.prismaService.currency.findMany({
      where: { code: { in: [cryptoCurrency, fiatCurrency] }, active: true },
    });

    if (!currencies || currencies.length < 2) {
      throw new BadRequestException('Invalid currencies provided');
    }

    const rate = await this.krakenService.getRate(
      `${cryptoCurrency}/${fiatCurrency}`,
    );

    if (!rate) {
      throw new BadRequestException('Rate currently unavailable for this pair');
    }

    return { rate };
  }
}
