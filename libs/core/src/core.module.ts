import { DynamicModule, Global, Module } from '@nestjs/common';
import { APP_INTERCEPTOR, APP_PIPE } from '@nestjs/core';
import { ConfigModule } from '@nestjs/config';

import { UnhandledErrorsInterceptor } from './interceptors';
import { TrimPipe } from './pipes';

@Global()
@Module({})
export class CoreModule {
  static forRoot(): DynamicModule {
    return {
      module: CoreModule,
      providers: [
        { provide: APP_PIPE, useClass: TrimPipe },
        { provide: APP_INTERCEPTOR, useClass: UnhandledErrorsInterceptor },
      ],
      imports: [ConfigModule.forRoot()],
    };
  }
}
