import { IsEnum } from 'class-validator';
import { CurrencyTypes } from '@prisma/client';

import { BaseListDto } from '@app/core';

export class GetCurrencyDto extends BaseListDto {
  @IsEnum(CurrencyTypes)
  type: CurrencyTypes;
}
