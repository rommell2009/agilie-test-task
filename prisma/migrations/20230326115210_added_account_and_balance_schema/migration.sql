-- CreateTable
CREATE TABLE "account" (
    "id" SERIAL NOT NULL,
    "crypto_currency" TEXT NOT NULL,
    "fiat_currency" TEXT NOT NULL,
    "balance" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "account_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "daily_balance" (
    "id" SERIAL NOT NULL,
    "crypto_currency" TEXT NOT NULL,
    "fiat_currency" TEXT NOT NULL,
    "balanceInCrypto" DOUBLE PRECISION NOT NULL,
    "balanceInFiat" DOUBLE PRECISION NOT NULL,
    "rate" DOUBLE PRECISION NOT NULL,
    "account_id" INTEGER NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "daily_balance_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "daily_balance" ADD CONSTRAINT "daily_balance_account_id_fkey" FOREIGN KEY ("account_id") REFERENCES "account"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
