import { IRate } from './rate.interface';

export interface IRatesService {
  getRate(cryptoCurrency: string, fiatCurrency: string): Promise<IRate>;
}
