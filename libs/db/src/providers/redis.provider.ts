import { Provider } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import IORedis from 'ioredis';

import { REDIS } from '../tokens';
import { dbConfig } from '../config';

export const RedisProvider: Provider = {
  provide: REDIS,
  useFactory: (config: ConfigType<typeof dbConfig>) => {
    return new IORedis(config.redis.connectionString);
  },
  inject: [dbConfig.KEY],
};
