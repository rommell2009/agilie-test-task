/*
  Warnings:

  - You are about to drop the column `balance` on the `account` table. All the data in the column will be lost.
  - You are about to drop the column `balanceInCrypto` on the `daily_balance` table. All the data in the column will be lost.
  - You are about to drop the column `balanceInFiat` on the `daily_balance` table. All the data in the column will be lost.
  - Added the required column `crypto_balance` to the `account` table without a default value. This is not possible if the table is not empty.
  - Added the required column `fiat_balance` to the `account` table without a default value. This is not possible if the table is not empty.
  - Added the required column `balance_in_crypto` to the `daily_balance` table without a default value. This is not possible if the table is not empty.
  - Added the required column `balance_in_fiat` to the `daily_balance` table without a default value. This is not possible if the table is not empty.
  - Added the required column `total_balance_in_crypto` to the `daily_balance` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "account" DROP COLUMN "balance",
ADD COLUMN     "crypto_balance" INTEGER NOT NULL,
ADD COLUMN     "fiat_balance" INTEGER NOT NULL;

-- AlterTable
ALTER TABLE "daily_balance" DROP COLUMN "balanceInCrypto",
DROP COLUMN "balanceInFiat",
ADD COLUMN     "balance_in_crypto" DOUBLE PRECISION NOT NULL,
ADD COLUMN     "balance_in_fiat" DOUBLE PRECISION NOT NULL,
ADD COLUMN     "total_balance_in_crypto" DOUBLE PRECISION NOT NULL;
