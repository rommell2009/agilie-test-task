import { Prisma, PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const currencyData: Prisma.CurrencyCreateInput[] = [
  {
    name: 'Bitcoin',
    code: 'BTC',
    precision: 8,
    active: true,
    type: 'CRYPTO',
  },
  {
    name: 'Bitcoin Cash',
    code: 'BCH',
    precision: 8,
    active: true,
    type: 'CRYPTO',
  },
  {
    name: 'Ethereum',
    code: 'ETH',
    precision: 8,
    active: true,
    type: 'CRYPTO',
  },
  {
    name: 'United States Dollar',
    code: 'USD',
    precision: 3,
    active: true,
    type: 'FIAT',
  },
  {
    name: 'Euro',
    code: 'EUR',
    precision: 3,
    active: true,
    type: 'FIAT',
  },
  {
    name: 'Canadian Dollar',
    code: 'CAD',
    precision: 3,
    active: true,
    type: 'FIAT',
  },
  {
    name: 'Japanese yen',
    code: 'JPY',
    precision: 3,
    active: true,
    type: 'FIAT',
  },
  {
    name: 'British pound',
    code: 'GBP',
    precision: 3,
    active: true,
    type: 'FIAT',
  },
  {
    name: 'Swiss franc',
    code: 'CHF',
    precision: 3,
    active: true,
    type: 'FIAT',
  },
  {
    name: 'Australian dollar',
    code: 'AUD',
    precision: 3,
    active: true,
    type: 'FIAT',
  },
];

const accountData: Prisma.AccountCreateInput[] = [
  {
    cryptoBalance: Math.random(),
    cryptoCurrency: 'BTC',
    fiatBalance: Math.floor(Math.random() * 1000),
    fiatCurrency: 'USD',
  },
  {
    cryptoBalance: Math.random(),
    cryptoCurrency: 'ETH',
    fiatBalance: Math.floor(Math.random() * 1000),
    fiatCurrency: 'EUR',
  },
  {
    cryptoBalance: Math.random(),
    cryptoCurrency: 'BCH',
    fiatBalance: Math.floor(Math.random() * 1000),
    fiatCurrency: 'GBP',
  },
];

async function main() {
  console.log(`Seeding started.`);

  for (const c of currencyData) {
    await prisma.currency.create({
      data: c,
    });
  }

  for (const a of accountData) {
    await prisma.account.create({
      data: a,
    });
  }

  console.log(`Seeding finished.`);
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
