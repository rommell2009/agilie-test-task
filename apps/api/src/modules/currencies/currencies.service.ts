import { Inject, Injectable } from '@nestjs/common';
import { Currency, CurrencyTypes } from '@prisma/client';

import { PRISMA, PrismaService } from '@app/db';

import { ICurrenciesService } from './interfaces';

@Injectable()
export class CurrenciesService implements ICurrenciesService {
  constructor(
    @Inject(PRISMA)
    private readonly prismaService: PrismaService,
  ) {}

  async getCurrencies(type: CurrencyTypes): Promise<Currency[]> {
    return this.prismaService.currency.findMany({
      where: { active: true, type },
    });
  }
}
