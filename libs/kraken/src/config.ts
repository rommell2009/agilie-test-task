import { registerAs } from '@nestjs/config';

export const kraken = registerAs('kraken', () => ({
  webSocketConnectionUrl: process.env.KRAKEN_WS_CONNECTION_URL,
}));
