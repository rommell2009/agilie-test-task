import { Inject, Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { Currency, CurrencyTypes } from '@prisma/client';
import { Redis } from 'ioredis';
import { WebSocket } from 'ws';

import { PRISMA, PrismaService, REDIS } from '@app/db';

import { IKrakenService } from './interfaces';
import { KRAKEN_SOCKET } from './tokens';

@Injectable()
export class KrakenService implements IKrakenService, OnModuleInit {
  private readonly logger = new Logger(KrakenService.name);

  constructor(
    @Inject(REDIS)
    private readonly redis: Redis,

    @Inject(PRISMA)
    private readonly prismaService: PrismaService,

    @Inject(KRAKEN_SOCKET)
    private readonly wsProvider: WebSocket,
  ) {}

  async onModuleInit() {
    await this.connect();
  }

  async getRate(pair: string): Promise<number> {
    return parseFloat(await this.redis.get(pair));
  }

  private async connect() {
    const currencies = await this.prismaService.currency.findMany({
      where: { active: true },
    });

    const pairs = this.createCurrencyPairs(currencies);

    const eventMessage = `{"event":"subscribe", "subscription":{"name":"ticker"}, "pair":${JSON.stringify(
      pairs,
    )}}`;

    this.wsProvider.on('open', () => {
      this.wsProvider.send(eventMessage);
    });

    this.wsProvider.on('message', (data: Buffer) => this.storeRates(data));

    this.wsProvider.on('close', () => {
      this.logger.warn(
        'Kraken socket connection closed. Trying to reconnect...',
      );

      return this.connect();
    });
  }

  private createCurrencyPairs(currencies: Currency[]): string[] {
    const cryptoCurrencies = currencies.filter(
      (c) => c.type === CurrencyTypes.CRYPTO,
    );
    const fiatCurrencies = currencies.filter(
      (c) => c.type === CurrencyTypes.FIAT,
    );

    const pairs = [];
    for (const cryptoCurrency of cryptoCurrencies) {
      for (const fiatCurrency of fiatCurrencies) {
        pairs.push(`${cryptoCurrency.code}/${fiatCurrency.code}`);
      }
    }

    return pairs;
  }

  private async storeRates(data: Buffer) {
    const parsedData = JSON.parse(data.toString());

    if (parsedData?.event === 'heartbeat') {
      return;
    }

    if (Array.isArray(parsedData) && parsedData[2] === 'ticker') {
      await this.redis.set(
        `${parsedData[3].replace('XBT', 'BTC')}`,
        parseFloat(parsedData[1].a[0]),
      );
    }
  }
}
