import { Controller, Get, Inject, Query } from '@nestjs/common';

import { GetCurrencyDto } from './dto';
import { ICurrenciesService } from './interfaces';
import { CURRENCIES_SERVICE } from './tokens';

@Controller('currencies')
export class CurrenciesController {
  constructor(
    @Inject(CURRENCIES_SERVICE)
    private readonly service: ICurrenciesService,
  ) {}

  @Get()
  async getCurrencies(@Query() dto: GetCurrencyDto) {
    return this.service.getCurrencies(dto.type);
  }
}
