import { Provider } from '@nestjs/common';

import { KrakenService } from '../kraken.service';
import { KRAKEN_SERVICE } from '../tokens';

export const KrakenServiceProvider: Provider = {
  provide: KRAKEN_SERVICE,
  useClass: KrakenService,
};
