import { Length } from 'class-validator';

export class GetRateDto {
  @Length(3, 3)
  cryptoCurrency: string;

  @Length(3, 3)
  fiatCurrency: string;
}
