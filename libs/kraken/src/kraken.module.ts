import { ConfigModule } from '@nestjs/config';
import { Module } from '@nestjs/common';

import { DBModule } from '@app/db';

import { kraken } from './config';
import { KRAKEN_PROVIDERS } from './providers';

@Module({
  imports: [ConfigModule.forFeature(kraken), DBModule],
  providers: [...KRAKEN_PROVIDERS],
  exports: [...KRAKEN_PROVIDERS],
})
export class KrakenModule {}
